﻿using ServiceCache.API.Models;
using System.Collections.Generic;

namespace ServiceCache.API.Services
{
    public interface ICacheService<T>
    {
        T Create(T item);
        List<T> Get();
        T Get(string id);
        void Remove(T value);
        void Remove(string id);
        void Update(string id, T value);
    }
}
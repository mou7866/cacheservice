﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using ServiceCache.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCache.API.Services
{
    public class CacheService : ICacheService<CachedItem>
    {
        private readonly IMongoCollection<CachedItem> _cachedItems;
        private readonly IOptions<CacheServiceSettings> _settings;

        public CacheService(IOptions<CacheServiceSettings> settings)
        {
            _settings = settings;
            var client = new MongoClient(_settings.Value.ConnectionString);
            var database = client.GetDatabase(settings.Value.DatabaseName);

            _cachedItems = database.GetCollection<CachedItem>(_settings.Value.CollectionName);
        }

        public List<CachedItem> Get() =>
            _cachedItems.Find(book => true).ToList();

        public CachedItem Get(string id) =>
            _cachedItems.Find<CachedItem>(item => item.Id == id).FirstOrDefault();

        public CachedItem Create(CachedItem item)
        {
            _cachedItems.InsertOne(item);
            return item;
        }

        public void Update(string id, CachedItem value)
        {
            _cachedItems.ReplaceOne(cachedItem => cachedItem.Id == id, value);
        }
        public void Remove(CachedItem value)
        {
            _cachedItems.DeleteOne(book => book.Id == value.Id);
        }

        public void Remove(string id)
        {
            _cachedItems.DeleteOne(book => book.Id == id);
        }
    }
}

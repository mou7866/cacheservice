﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceCache.API.Models
{
    public class CachedItem
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [BsonElement("Item")]
        [JsonProperty("Item")]
        [BsonRepresentation(BsonType.String)]
        public string Item { get; set; }
    }
}

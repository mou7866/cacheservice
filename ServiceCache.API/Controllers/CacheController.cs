﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceCache.API.Models;
using ServiceCache.API.Services;

namespace ServiceCache.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CacheController : ControllerBase
    {
        private readonly ICacheService<CachedItem> _cacheService;

        public CacheController(ICacheService<CachedItem> cacheService)
        {
            _cacheService = cacheService;
        }

        [HttpGet]
        public ActionResult<List<CachedItem>> Get()
        {
            return _cacheService.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetCache")]
        public ActionResult<CachedItem> Get(string id)
        {
            var cachedItem = _cacheService.Get(id);

            if (cachedItem == null)
            {
                return NotFound();
            }

            return cachedItem;
        }

        [HttpPost]
        public ActionResult<CachedItem> Create(CachedItem cachedItem)
        {
            _cacheService.Create(cachedItem);

            return CreatedAtRoute("GetCache", new { id = cachedItem.Id.ToString() }, cachedItem);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, CachedItem cachedItem)
        {
            var book = _cacheService.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            _cacheService.Update(id, cachedItem);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var book = _cacheService.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            _cacheService.Remove(book.Id);

            return NoContent();
        }
    }
}